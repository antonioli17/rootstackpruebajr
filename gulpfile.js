var gulp    = require('gulp');
var browser = require('browser-sync').create();
var sass    = require('gulp-sass');

var port = process.env.SERVER_PORT || 3000;

// personally written JS
var jsFiles = [
  'node_modules/bootstrap/dist/js/bootstrap.min.js',
  'node_modules/jquery/dist/jquery.min.js',
  'node_modules/popper.js/dist/umd/popper.min.js',
]

// personally written CSS
var cssFiles = [
  'node_modules/bootstrap/scss/bootstrap.scss',
  'src/scss/*.scss'
]

// concacts and minifys all personally written JS
gulp.task('scripts', function() {
  var stream = gulp.src(jsFiles)
    .pipe(gulp.dest('src/js'));
  return stream;
});

// Minify personally written css to at least ie8 compatibility
gulp.task('stylesheets', function() {
  var stream = gulp.src(cssFiles)
    .pipe(sass())
    .pipe(gulp.dest('src/css'));
  return stream;
})

// Bring up the browser and serve app
gulp.task('browser', function() {
  browser.init({
    server: 'src/',
    port: port
  });
  // watch and rebuild .js files
  gulp.watch(jsFiles, gulp.parallel('scripts'))
    .on('change', browser.reload);

  // watch and rebuild .css files
  gulp.watch(cssFiles, gulp.parallel('stylesheets'))
    .on('change', browser.reload);

  // Reload when html changes
  gulp.watch('src/*.html')
    .on('change', browser.reload);
})

// Clean is forced to run *FIRST* using gulp.series
// Then subsequent tasks can be asynchronous in executing
gulp.task('serve', gulp.series(
  gulp.parallel(
    'scripts',
    'stylesheets'),
  'browser'));

// attach a default task, so when when just <code>gulp</code> the thing runs
gulp.task('default', gulp.series('serve'));