# Prueba JR - Rootstack

## Objetivo

El test sería implementar el HTML de la imagen que te adjunto.

## Aspectos a evaluar:

* Limpieza/aseo de los archivos HTML y CSS.
* Semantica del HTML y CSS.
* Similitud con el diseño original.
* Buen criterio a la hora de decidir la estructura: que lo que se supone que son textos, sean textos. Los botones son flexibles, etc.
* Soporte a navegadores actuales.
* Como manejas el diseño responsivo.

## Reglas

* No usar ID para estilos
* SCSS/LESS/CSS deben mostrar una estructura (no tirados en un archivo simplemente)

## Que buscamos

* Mejores prácticas
* Código semántico
* Que herramientas usas
* Moduladirad del código

## Que no estamos buscando:

* Un diseño perfecto
* Un template hecho por otro desarrollador
 
## Tipografias usadas:
Las puedes sacar https://fonts.google.com/.

- Roboto Medium
- Oswald Medium

Titulo principal - 48px